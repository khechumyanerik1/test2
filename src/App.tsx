import React, { useState, useEffect } from 'react';
import { Redirect, Route, Switch, BrowserRouter } from 'react-router-dom';
import { LoginPage } from './pages/LoginPage';
import { ConfirmPage } from './pages/ConfirmPage';




export default function App() {
    const [redirect, setRedirect] = useState(false);
    const [email, setEmail] = useState('');

    useEffect(() => {
        if (!redirect) {
            setRedirect(true);
        }
    }, []);

    if (!redirect) {
        return (
            <BrowserRouter>
                <Redirect to="/login/step-1" />
            </BrowserRouter>
        );
    }

    return (
        <BrowserRouter>
            <div>
                <header className="h-20 bg-primary flex items-center p-4">
                    <h1 className="text-3xl text-black">Title</h1>
                </header>
                <main className="flex flex-col p-4 h-full">
                    <Switch>
                        <Route path="/login/step-1">
                            <LoginPage email={email} setEmail={setEmail} />
                        </Route>
                        <Route path="/login/step-2">
                            <ConfirmPage email={email}/>
                        </Route>
                    </Switch>
                </main>
            </div>
        </BrowserRouter>
    );
}






