import { useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import { emailIsValid } from '../helpers/emailIsValid';
import { FormInput } from '../ui/FormInput';
import { FormCheckbox } from '../ui/FormCheckbox';
import { Button } from '../ui/Button';

interface LoginPageProps {
    email: string;
    setEmail: React.Dispatch<React.SetStateAction<string>>;
}

export function LoginPage({email, setEmail}: LoginPageProps) {
    const [isChecked, setIsChecked] = useState(false);

    const disableButton = !isChecked || !emailIsValid(email)

    return (
        <Switch>
            <Route path="/login">
                <div>
                    <FormInput setEmail={setEmail} />
                    <div className="p-1"></div>
                    <FormCheckbox setIsChecked={setIsChecked} />
                    <Button disableButton={disableButton}/>
                </div>
            </Route>
            <Route path="/not-implemented">Not implemented</Route>
            {/* Add more routes as needed */}
        </Switch>
    );
}