import axios from 'axios';
import { Route, useHistory } from 'react-router-dom';
import { Popup } from '../ui/Popup';
import { useState } from 'react';

interface ConfirmPageProps {
    email: string;
}

export function ConfirmPage({ email }: ConfirmPageProps) {

    const history = useHistory();
    const [popupText, setPopupText] = useState('')

    return (

        <Route path="/login">
            <div>
                <label className="form-control">
                    <div className="label">
                        <span className="label-text">Email</span>
                    </div>
                    <input
                        type="text"
                        placeholder="Type here"
                        className="input"
                        value={email}
                        disabled
                    />
                </label>
                <div className="p-1"></div>
                <div className='flex justify-between mt-auto gap-4 w-[340px] absolute top-[calc(100vh-30px)]'>
                    <button className="w-1/2 bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" onClick={() => {
                        history.goBack();
                    }}>back</button>
                    <button className="w-1/2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        onClick={() => {
                            axios.post('/api/endpoint', { email }).then((res) => {
                                if (res.status === 200) {
                                    setPopupText('Success!')
                                } else {
                                    setPopupText('Error!')
                                }
                            })
                        }}>confirm</button>
                </div>
                <Popup popupText={popupText} setPopupText={setPopupText} />
            </div>
        </Route>
    );
}