
interface FormInputProps {
    setEmail: React.Dispatch<React.SetStateAction<string>>;
}

export function FormInput({ setEmail }: FormInputProps) {
    const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
    };

    return (
        <label className="form-control">
            <div className="label">
                <span className="label-text">Email</span>
            </div>
            <input
                type="text"
                placeholder="Type here"
                className="input"
                onChange={handleEmailChange}
            />
        </label>
    );
}