interface FormCheckboxProps {
    setIsChecked: React.Dispatch<React.SetStateAction<boolean>>;
}

export function FormCheckbox({ setIsChecked }: FormCheckboxProps) {
    const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setIsChecked(event.target.checked);
    };

    return (
        <div className="form-control">
            <label className="label cursor-pointer justify-start gap-2">
                <input
                    type="checkbox"
                    className="checkbox checkbox-primary"
                    onChange={handleCheckboxChange}
                />
                <span className="label-text">I agree</span>
            </label>
        </div>
    );
}