import { useState, useRef, useEffect } from 'react';
import { useHistory } from 'react-router';

interface ButtonProps {
    disableButton: boolean;
}

export const Button = ({ disableButton = true }: ButtonProps) => {
    const defaultTimeMS = 500;
    const [timer, setTimer] = useState(defaultTimeMS)
    const holdTimeoutRef = useRef<any>(null);
    const history = useHistory();
    const timeInPercent = timer*100 / defaultTimeMS

    useEffect(() => {
        if (timer <= 0) {
            history.push('/login/step-2');
        }
    }, [timer, history]);

    const handleMouseDown = () => {
        holdTimeoutRef.current = setInterval(() => {
            setTimer(prev => prev - 100)
        }, 100)
    };

    const handleMouseUp = () => {
        clearInterval(holdTimeoutRef.current)
        setTimer(defaultTimeMS)
    };

    return (
        <button
            disabled={disableButton}
            className={`relative bg-blue-500 text-white font-bold py-2 px-4 rounded text-primary bg-secondary w-full ${disableButton ? 'opacity-50' : 'hover:bg-blue-700'}`}
            onMouseDown={handleMouseDown}
            onMouseUp={handleMouseUp}
            onMouseLeave={handleMouseUp} // Handle case when mouse leaves the button while holding
        >
            {'Hold to proceed'}
            <span style={{
                width: 100 - timeInPercent + '%'
            }} className="absolute bottom-0 left-0 h-1 bg-white transition-all duration-100 ease-linear"></span>
        </button>
    );
};
